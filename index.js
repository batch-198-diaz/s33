// index.js


/*
1. express package was imported as constant express
2. invoked express package to create a server/api and saved in a variable which can be referred to later to create routes
3. variable for port assignment (but can also be omitted? or is optional)
4. get() method to create a route in express
5. listen() method of express to assign a port to our server and send a message

app.use() is a method from express that handles the stream of data from our client and receives the 


*/

const express = require("express");

const app = express();

app.use(express.json())

const port = 4000;



app.get('/',(req,res) =>{

	res.send("Hello from our first ExpressJS route!");
})

app.post('/',(req,res) =>{

	res.send("Hello from our first ExpressJS Post Method Route!")
})

app.put('/',(req,res) =>{

	res.send("Hello from a Put Method Route!")
})

app.delete('/',(req,res) =>{

	res.send("Hello from a Delete Method Route!")
})



let courses = [

	{
		name: "Python 101",
		description: "Learn Python",
		price: 25000
	},
	{
		name: "ReactJS 101",
		description: "Learn React",
		price: 35000
	},
	{
		name: "ExpressJS 101",
		description: "Learn ExpressJS",
		price: 28000
	}

];


app.get('/courses',(req,res) =>{

	res.send(courses);
})

app.post('/courses',(req,res) =>{

	// console.log(req.body);

	courses.push(req.body);

	// console.log(courses);

	res.send(courses);
})

/* ====================== ACTIVITY ====================== */

let users = [

	{
		email: "marybell_knight",
		password: "marrymarybell"
	},
	{
		email: "janedeoPriest",
		password: "jobPriest100"	
	},
	{
		email: "kimTofu",
		password: "dubuTofu"		
	}
];

app.get('/users',(req,res) =>{

	res.send(users);
})

app.post('/users',(req,res) =>{

	users.push(req.body);
	res.send(users);
})

// stretch goal #1

app.delete('/users',(req,res) =>{

	users.pop(req.body);
	res.send("Last added user has been deleted.");
	// res.send(users);
})

// stretch goal #2

app.put('/courses',(req,res) =>{

	// let data = req.body;
	// let index = courses.map(object => object.name).indexOf(data.name);

	// let courseUpdate = courses[index];
	// courseUpdate.price = data.price;
	// res.send(courses)

	let data = req.body;
	let courseUpdate = courses[data.index];
	courseUpdate.price = data.price;
	res.send(courses);

})




app.listen(port,() => console.log(`Express API running at port 4000`))
